#FEDD Project - Educational Computer Game

#Group members

| Group Member   | Role            |
|----------------|-----------------|
| Kaveen Herath  | Developer       |
| Akhil Acharya  | Developer       |
| Collin Benoit  | Project Manager |
| Thomas Keeshan | Artist          |


